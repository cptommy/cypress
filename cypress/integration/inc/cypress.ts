/// <reference types="cypress" />
// @ts-check

/**
 * redirect to the start page
 */
export function goToStartPage()
{
    cy.log('goToStartPage start');
    cy.get('@settingsClient').then((settings:any) => {
        cy.visit(settings.baseUrl);
    });
    cy.log('goToStartPage end');
}

/**
 * set browser in mobile mode
 * @param cy
 * @param Cypress
 */
export function setViewPortMobile(cy:Cypress.cy, Cypress)
{
    //set screen size
    cy.viewport('iphone-x');
}

/**
 * call the shop and tell it, the test beginn
 * @param cy
 * @param Cypress
 */
export function before(cy:Cypress.cy, Cypress){

    cy.log('before start');

    let configFile = Cypress.env('configFile');
    configFile = configFile + '.json';

    //load basic settings for this developer
    cy.fixture(configFile).as('settingsClient');

    //tell shop we start testing with cypress, so the shop can maybe make some changes during testing
    //e.g. set the paypal module for this session into sandbox mode so no real purchase will make....
    cy.get('@settingsClient').then((settings:any) => {

        let url = new URL(settings.baseUrl + "/index.php");
        url.searchParams.append('cl','cpcypress');
        url.searchParams.append('fnc','cpcypress_init');
        url.searchParams.append('test',Cypress.spec.name);

        cy.log('init shop: ' + url.href);
        cy.server();
        cy.request({
            method: 'GET',
            url: url.href
        }).then((response) => {
            console.log(response);
            cy.log(String(response.status));
        });
    });

    cy.log('before start');
}

/**
 * do following before each test
 *
 * @param cy
 * @param Cypress
 */
export function beforeEach(cy:Cypress.cy, Cypress) {
    cy.log('beforeEach start');

    cy.clearCookies();
    cy.clearLocalStorage();

    cy.log('beforeEach end');
}

/**
 * do following after each test
 *
 * @param cy
 * @param Cypress
 */
export function afterEach(cy:Cypress.cy, Cypress) {
    cy.log('afterEach start');

    cy.log('afterEach end');
}

/**
 * after the test, call the shop to clean up
 *
 * @param cy
 * @param Cypress
 */
export function after(cy:Cypress.cy, Cypress) {

    cy.log('after start');

    //tell shop we stop testing with cypress, so the shop can maybe make some clean up after testing
    //e.g. clean session, remove log data...
    cy.get('@settingsClient').then((settings:any) => {

        let url = new URL(settings.baseUrl + "/index.php");
        url.searchParams.append('cl','cpcypress');
        url.searchParams.append('fnc','cpcypress_terminate');
        url.searchParams.append('test',Cypress.spec.name);

        cy.log('url: ' + url.href);
        cy.server();
        cy.request({
            method: 'GET',
            url: url.href
        }).then((response) => {
            console.log(response);
            cy.log(String(response.status));
        });
    });

    cy.log('after end');
}
