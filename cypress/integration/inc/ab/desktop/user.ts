/// <reference types="cypress" />
// @ts-check

/// <reference types="cypress" />
// @ts-check

/**
 *
 * @param cy
 * @param Cypress
 * @param data
 */
export function createUser(cy:Cypress.cy, Cypress, data:any)
{
    cy.log('createUser start');
    cy.get('@settingsClient').then((settings:any) => {
        cy.visit(settings.baseUrl);

        //go to registration page (header)
        cy.get(':nth-child(1) > .cp-btn-header-lg').click();
        cy.get('.dd-login > :nth-child(2) > .cp-btn-white').click();


        //fill form
        cy.get('form[name=order] input[name="cpUser[oxuser__cpfname]"]').first().type(data.oxuser__oxfname);
        cy.get('form[name=order] input[name="cpUser[oxuser__cplname]"]').first().type(data.oxuser__oxlname);
        cy.get('form[name=order] input[name="cpUser[oxuser__cpfon]"]').first().type(data.oxuser__oxfon);
        cy.get('form[name=order] input[type=text][name=lgn_usr]').first().type(data.oxuser__oxusername);
        cy.get('form[name=order] input[type=password][name=lgn_pwd]').first().type(data.oxuser__oxpassword);
        cy.get('form[name=order] input[type=password][name=lgn_pwd2]').first().type(data.oxuser__oxpassword);
        cy.get('form[name=order] button[type=submit]').first().click();

        cy.visit(settings.baseUrl);
    });
    cy.log('createUser end');
}

