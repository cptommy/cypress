/// <reference types="cypress" />
// @ts-check

/**
 * @param cy
 * @param Cypress
 * @param sku
 */
export function search(cy, Cypress, sku) {
    cy.get('form[name=search] input[name=searchparam]').type(sku);
    cy.get('form[name=search] button').click();
}

/**
 *
 * @param cy
 * @param Cypress
 * @param sku
 * @param amount
 */
export function addToBasket(cy, Cypress, sku,  amount)
{
    search(cy, Cypress, sku);

    cy.get('div#productinfo div.productMainInfo div.main-container input[name=am]').clear().type(amount);
    cy.get('div#productinfo div.productMainInfo div.main-container button#addToCart').click();
}
