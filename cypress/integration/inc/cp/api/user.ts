/// <reference types="cypress" />
// @ts-check

/**
 * create a user directly in the database
 *
 * @param cy
 * @param Cypress
 * @param data
 */
export function createUser(cy:Cypress.cy, Cypress, data:any)
{
    cy.log('apiDeleteUser start');
    cy.get('@settingsClient').then((settings:any) => {

        let url = new URL(settings.baseUrl + "/index.php");
        url.searchParams.append('cl','cpcypress');
        url.searchParams.append('fnc','cpcypress_createUser');
        url.searchParams.append('test',Cypress.spec.name);
        url.searchParams.append('data', JSON.stringify(data));
        cy.log(url.href);

        cy.server();
        cy.request({
            method: 'POST',
            url: url.href
        }).then((response) => {
            console.log(response);
            cy.log(String(response.status));
        });

        cy.visit(settings.baseUrl);
    });
    cy.log('apiDeleteUser end');
}

/**
 * remove a user and its addresses and order from the database. also the user will log out in the shop.
 *
 * @param cy
 * @param Cypress
 * @param data
 */
export function deleteUser(cy:Cypress.cy, Cypress, data:any)
{
    cy.log('apiDeleteUser start');
    cy.get('@settingsClient').then((settings:any) => {

        let url = new URL(settings.baseUrl + "/index.php");
        url.searchParams.append('cl','cpcypress');
        url.searchParams.append('fnc','cpcypress_deleteUser');
        url.searchParams.append('test',Cypress.spec.name);
        url.searchParams.append('oxuser__oxusername', data.oxuser__oxusername);
        cy.log(url.href);

        cy.server();
        cy.request({
            method: 'GET',
            url: url.href
        }).then((response) => {
            console.log(response);
            cy.log(String(response.status));
        });

        cy.visit(settings.baseUrl);
    });
    cy.log('apiDeleteUser end');
}
