/// <reference types="cypress" />
// @ts-check

/**
 * @param cy
 * @param Cypress
 * @param sku
 */
export function search(cy, Cypress, sku) {
    cy.get('button[data-cp-action=cp-open-search]').click();
    cy.get('form#cp-header-search-form input[type=text]').clear().type(sku);
    cy.get('form#cp-header-search-form button').click();
}

/**
 *
 * @param cy
 * @param Cypress
 * @param sku
 * @param amount
 */
export function addToBasket(cy, Cypress, sku,  amount)
{
    search(cy, Cypress, sku);

    cy.get('input#cp-to-basket-amt').scrollIntoView().clear().type(amount);
    cy.get('button#cp-to-cart').click();
    cy.wait(2000);
}
