/// <reference types="cypress" />
// @ts-check

/**
 *
 * @param cy
 * @param Cypress
 * @param data
 */
export function createUser(cy:Cypress.cy, Cypress, data:any)
{
    cy.log('createUser start');
    cy.get('@settingsClient').then((settings:any) => {
        cy.visit(settings.baseUrl);

        //go to registration page (header)
        cy.get('div#oxwidget_headerlogin').trigger('mouseover');
        cy.get('div#oxwidget_headerlogin a').first().click();

        //fill form
        cy.get('form[name=order] input[type=text][name=lgn_name]').type(data.oxuser__oxfname);
        cy.get('form[name=order] input[type=text][name=lgn_usr]').type(data.oxuser__oxusername);
        cy.get('form[name=order] input[type=password][name=lgn_pwd]').type(data.oxuser__oxpassword);
        cy.get('form[name=order] input[type=password][name=lgn_pwd2]').type(data.oxuser__oxpassword);
        cy.get('form[name=order] button[type=submit]').first().click();

        cy.visit(settings.baseUrl);
    });
    cy.log('createUser end');
}

