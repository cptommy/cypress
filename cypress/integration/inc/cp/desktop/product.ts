/// <reference types="cypress" />
// @ts-check

/**
 * @param cy
 * @param Cypress
 * @param sku
 */
export function search(cy, Cypress, sku) {
    cy.get('form[name=search] input[name=searchparam]').type(sku);
    cy.get('form[name=search] button').click();
}

/**
 *
 * @param cy
 * @param Cypress
 * @param sku
 * @param amount
 */
export function addToBasket(cy, Cypress, sku,  amount)
{
    search(cy, Cypress, sku);

    cy.get('div.detailsInfo div.embuttonamount_amount input[type=text]').clear().type(amount);
    cy.get('div.detailsInfo_pricebox_addBasket .basketButton .submitButton').click();
}
