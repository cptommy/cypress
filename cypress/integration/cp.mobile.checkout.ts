/// <reference types="cypress" />
// @ts-check

import * as cpcypress from "./inc/cypress";
import * as user from './inc/cp/api/user';
import * as product from './inc/cp/mobile/product';


describe('checkout', function() {

    let testdata = {

        "orderSku" : "0000-TEST",
        "orderAmount" : 2,

        "oxuser__oxusername" : 'cypress@cyberpuerta.mx',
        "oxuser__oxpassword" : 'cypress@cyberpuerta.mx',
        "oxuser__oxfname" : 'Cypress',

        "oxaddress__oxfname" : 'oxfname',
        "oxaddress__oxlname" : 'oxlname',
        "oxaddress__oxfon" : '0123456789',
        "oxaddress__oxstreet" : 'oxstreet',
        "oxaddress__oxstreetnr" : 'oxstreetnr',
        "oxaddress__emcolonia" : 'emcolonia',
        "oxaddress__oxzip" : '47700',
        "oxaddress__oxcity" : 'oxcity',
        "oxaddress__oxsal" : 'MR',
        "oxaddress__oxstateid" : '4f4ae03cb0756', //Jalisco
        "oxaddress__oxcountry" : 'México',
        "oxaddress__oxcountryid" : '8f241f11095ebf3a6.86388577',
    };


    //before a test start, do following
    before(() => {
        cy.log('before all tests');
        cpcypress.before(cy, Cypress);
    });

    //before each test, do following
    beforeEach(function() {
        cy.log('before each test');
        cpcypress.beforeEach(cy, Cypress);
    });





    it('Checkout', function() {

        cpcypress.setViewPortMobile(cy, Cypress);

        user.createUser(cy, Cypress, testdata);

        product.addToBasket(cy, Cypress, testdata.orderSku, testdata.orderAmount);

        //basket should display now, click to the checkout
        cy.get('div#cp-mini-basket-menu button[data-bind="click: goToCheckout"]').click();
        cy.wait(1000);

        //login
        cy.get('input[name=lgn_usr]').type(testdata.oxuser__oxusername);
        cy.get('input[name=lgn_pwd]').type(testdata.oxuser__oxpassword);
        cy.get('form#cp-global-login-form button').first().click();
        cy.wait(5000);

        //next 1 de 3
        cy.get('section.activeview button[data-bind="click: goNextStep"]').first().click();

        //select estafeta, oxxo
        cy.get('input[type=radio][value=oxidstandard]').parent().click();
        cy.get('input[type=radio][name=paymentid][value="cpoxxopay"]').parent().click();

        //next 2 de 3
        cy.get('button#paymentNextStepBottom').click();

        //order!!
        cy.get('form#orderConfirmAgbTop button').click();

        cy.get('div#thankyouPage div.thankyou_box div span').invoke('text').then(function(text) {
            let orderNr=text.trim();
            expect(orderNr).contain('CP');
        });

        user.deleteUser(cy, Cypress, testdata);
    });





    //after each test, do following
    afterEach(function() {
        cy.log('after each test');
        cpcypress.afterEach(cy, Cypress);
    });

    //after all test do following
    after(function() {
        cy.log('after all tests');
        cpcypress.after(cy, Cypress);

    });

});

