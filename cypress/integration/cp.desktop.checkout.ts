/// <reference types="cypress" />
// @ts-check

import * as cpcypress from "./inc/cypress";
import * as user from './inc/cp/desktop/user';
import * as userApi from './inc/cp/api/user';
import * as product from './inc/cp/desktop/product';


describe('checkout', function() {

    let testdata = {

        "orderSku" : "0000-TEST",
        "orderAmount" : 2,

        "oxuser__oxusername" : 'cypress@cyberpuerta.mx',
        "oxuser__oxpassword" : 'cypress@cyberpuerta.mx',
        "oxuser__oxfname" : 'Cypress',

        "oxaddress__oxfname" : 'oxfname',
        "oxaddress__oxlname" : 'oxlname',
        "oxaddress__oxfon" : '0123456789',
        "oxaddress__oxstreet" : 'oxstreet',
        "oxaddress__oxstreetnr" : 'oxstreetnr',
        "oxaddress__emcolonia" : 'emcolonia',
        "oxaddress__oxzip" : '47700',
        "oxaddress__oxcity" : 'oxcity',
        "oxaddress__oxsal" : 'MR',
        "oxaddress__oxstateid" : '4f4ae03cb0756', //Jalisco
        "oxaddress__oxcountry" : 'México',
        "oxaddress__oxcountryid" : '8f241f11095ebf3a6.86388577',
    };


    //before a test start, do following
    before(() => {
        cy.log('before all tests');
        cpcypress.before(cy, Cypress);
    });

    //before each test, do following
    beforeEach(function() {
        cy.log('before each test');
        cpcypress.beforeEach(cy, Cypress);
    });





    it('Checkout', function() {

        user.createUser(cy, Cypress, testdata);

        product.addToBasket(cy, Cypress, testdata.orderSku, testdata.orderAmount);

        //go to the basket page
        cy.get('div#header a.oxwidget_headerminibasket_header').click();

        //go to the user page
        cy.get('div.embasket_nextstep_button button').click();

        //complete user registration (delivery address)
        cy.get('div.cpck-address div[data-cp-fill-delivery-address] form.address-form input[name=oxaddress__oxfname]').type(testdata.oxaddress__oxfname);
        cy.get('div.cpck-address div[data-cp-fill-delivery-address] form.address-form input[name=oxaddress__oxlname]').type(testdata.oxaddress__oxlname);
        cy.get('div.cpck-address div[data-cp-fill-delivery-address] form.address-form input[name=oxaddress__oxfon]').type(testdata.oxaddress__oxfon);
        cy.get('div.cpck-address div[data-cp-fill-delivery-address] form.address-form input[name=oxaddress__oxstreet]').type(testdata.oxaddress__oxstreet);
        cy.get('div.cpck-address div[data-cp-fill-delivery-address] form.address-form input[name=oxaddress__oxstreetnr]').type(testdata.oxaddress__oxstreetnr);
        cy.get('div.cpck-address div[data-cp-fill-delivery-address] form.address-form input[name=oxaddress__emcolonia]').type(testdata.oxaddress__emcolonia);
        cy.get('div.cpck-address div[data-cp-fill-delivery-address] form.address-form input[name=oxaddress__oxzip]').type(testdata.oxaddress__oxzip);
        cy.get('div.cpck-address div[data-cp-fill-delivery-address] form.address-form input[name=oxaddress__oxcity]').type(testdata.oxaddress__oxcity);
        cy.get('div.cpck-address div[data-cp-fill-delivery-address] form.address-form input[name=oxaddress__oxfname]').type(testdata.oxaddress__oxfname);
        cy.get('div.cpck-address div[data-cp-fill-delivery-address] form.address-form select[name=oxaddress__oxstateid]').select(testdata.oxaddress__oxstateid);
        //complete user registration (billing address)
        cy.get('div.cpck-address div[data-cp-fill-delivery-address] form.address-form button').click();
        //go to the payment page
        cy.get('div.cpck-address div[data-cp-fill-billing-data] button').click();

        //select estafeta
        cy.get('div.cp-del-set-groups input[type=radio][value=oxidstandard]').parent().click();
        cy.get('ul#paymentlist li[data-paymentid=cpoxxopay]').click();

        //go to order overview
        cy.get('button#paymentNextStepBottom').click();

        //order!
        cy.get('form#orderConfirmAgbTop button').click();

        cy.get('div#thankyouPage div.thankyou_box div span').invoke('text').then(function(text) {
            let orderNr=text.trim();
            expect(orderNr).contain('CP');
        });

        userApi.deleteUser(cy, Cypress, testdata);
    });





    //after each test, do following
    afterEach(function() {
        cy.log('after each test');
        cpcypress.afterEach(cy, Cypress);
    });

    //after all test do following
    after(function() {
        cy.log('after all tests');
        cpcypress.after(cy, Cypress);

    });

});

