/// <reference types="cypress" />
// @ts-check

import * as cpcypress from "./inc/cypress";
import * as user from "./inc/ab/desktop/user";
import * as product from "./inc/ab/desktop/product";
import * as userApi from "./inc/cp/api/user";

describe('checkout', function() {

    let testdata = {

        "orderSku" : "0000-TEST",
        "orderAmount" : 2,

        "oxuser__oxusername" : 'cypress@cyberpuerta.mx',
        "oxuser__oxpassword" : 'cypress@cyberpuerta.mx',
        "oxuser__oxfname" : 'Cypress',
        "oxuser__oxlname" : 'Cypress',
        "oxuser__oxfon" : '1234567890',

        "oxaddress__oxfname" : 'oxfname',
        "oxaddress__oxlname" : 'oxlname',
        "oxaddress__oxfon" : '0123456789',
        "oxaddress__oxstreet" : 'oxstreet',
        "oxaddress__oxstreetnr" : '2',
        "oxaddress__cpcolonia" : 'cpcolonia',
        "oxaddress__oxzip" : '47700',
        "oxaddress__oxcity" : 'oxcity',
        "oxaddress__oxsal" : 'MR',
        "oxaddress__oxstateid" : '4f4ae03cb0756', //Jalisco
        "oxaddress__oxcountry" : 'México',
        "oxaddress__oxcountryid" : '8f241f11095ebf3a6.86388577',
    };


    //before a test start, do following
    before(() => {
        cy.log('before all tests');
        cpcypress.before(cy, Cypress);
    });

    //before each test, do following
    beforeEach(function() {
        cy.log('before each test');
        cpcypress.beforeEach(cy, Cypress);
    });





    it('Checkout', function() {

        userApi.deleteUser(cy, Cypress, testdata);

        cpcypress.goToStartPage();

        //create user
        user.createUser(cy, Cypress, testdata);

        //add product to basket
        product.addToBasket(cy, Cypress, testdata.orderSku, testdata.orderAmount);

        //go to basket page
        cy.get('.cp-mini-cart-link').click();

        //go to user page
        cy.get('form > .cp-btn-ab').click();

        //complete user registration (delivery address)
        cy.get('div[data-cp-fill-delivery-address] form input[name=oxaddress__oxfname]').type(testdata.oxaddress__oxfname);
        cy.get('div[data-cp-fill-delivery-address] form input[name=oxaddress__oxlname]').type(testdata.oxaddress__oxlname);
        cy.get('div[data-cp-fill-delivery-address] form input[name=oxaddress__oxfon]').type(testdata.oxaddress__oxfon);
        cy.get('div[data-cp-fill-delivery-address] form input[name=oxaddress__oxstreet]').type(testdata.oxaddress__oxstreet);
        cy.get('div[data-cp-fill-delivery-address] form input[name=oxaddress__oxstreetnr]').type(testdata.oxaddress__oxstreetnr);
        cy.get('div[data-cp-fill-delivery-address] form input[name=oxaddress__cpcolonia]').type(testdata.oxaddress__cpcolonia);
        cy.get('div[data-cp-fill-delivery-address] form input[name=oxaddress__oxzip]').type(testdata.oxaddress__oxzip);
        cy.get('div[data-cp-fill-delivery-address] form input[name=oxaddress__oxcity]').type(testdata.oxaddress__oxcity);
        cy.get('div[data-cp-fill-delivery-address] form select[name=oxaddress__oxstateid]').select(testdata.oxaddress__oxstateid);
        cy.get('div[data-cp-fill-delivery-address] form button').click();

        //complete billing data
        cy.get('[data-cp-section=""] > :nth-child(2) > .small-3 > .cp-btn-ab').click();

        //select estafeta
        cy.get('input[type="radio"][value="oxidstandard"]').parent().click();
        cy.wait(3000);

        //select bank transfer
        cy.get('input#payment_cptransfer').next().click();
        cy.wait(3000);

        //go to order overview page
        cy.get('button#paymentNextStepBottom').click();

        //really order
        cy.get('button#order-button').click();

        cy.get('div.order-number-box span').invoke('text').then(function(text) {
            let orderNr=text.trim();
            expect(orderNr).contain('AB');
        });

        //clean up
        userApi.deleteUser(cy, Cypress, testdata);
    });





    //after each test, do following
    afterEach(function() {
        cy.log('after each test');
        cpcypress.afterEach(cy, Cypress);
    });

    //after all test do following
    after(function() {
        cy.log('after all tests');
        cpcypress.after(cy, Cypress);

    });

});

