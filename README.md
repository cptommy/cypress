INSTALL
=

Install last node on you computer.

    https://nodejs.org/en/download/
    
Open terminal in the project folder and execute

    npm install --save-dev
    
It will install "Cypress" and "TypeScript" on your pc.

Configuration
=

Cypress has to know where it should connect to. To help Cypress, create your own JSON file in 

    cypress\fixtures
    
like the example files. You have to create a JSON file for each shop you like to connect and execute test.

Schema:

    #SHOP#.#DEVELOPER NAME#.json
    
Example:

    cp.tommy.json
    ab.tommy.json
    
![](readme/settings1.png)


As content for the JSON file use following as blueprint or use an existing JSON file and modify the values in.

    {
        "baseUrl":"http://clone:clone@tommy.cyberpuerta.mx"
    }

Please replace the link to the developer shop. If you use the docker environment, you have only to add new shops. 
The docker URL is always the same for each user and you can use the existing files.

    ab.docker.json

Test scripts
=

The test will run on your own computer.

All tests which present in the shop located in the folder

    cypress\integration\*
    
The schema for the file name is:

    #SHOP#.#DEVICE#.#DESCRIPTION#.js
    
For example:

    cp.desktop.checkout.js
    cp.mobile.checkout.js
    ab.desktop.checkout.js

CLI
=
    
To run a test you have to execute Cypress with some command line arguments.

    --project = relative path to the cypress directory located in this project. Simply "."
    --env = filename with basic settings where Cypress has to know. (file in fixtures folder)
    --spec = relative path to the test file which should execute
    --config = some configurations Cypress need to start in the correct mode like the user agent for mobile testing
    
Schema:

    npx cypress run --env configFile="#YOUR SETTINGS FILE YOU LIKE TO USE WITHOUT EXTENSION#" --project "." --spec "./cypress/integration/#FILENAME OF THE TEST TS FILE#" --config userAgent="smartfon"
    
Example desktop:

    npx cypress run --env configFile="cp.tommy" --project "." --spec "./cypress/integration/cp.desktop.checkout.ts"
    npx cypress run --env configFile="ab.tommy" --project "." --spec "./cypress/integration/ab.desktop.checkout.ts"

Example mobile:

    npx cypress run --env configFile="cp.tommy" --project "." --spec "./cypress/integration/cp.mobile.checkout.ts" --config userAgent="smartfon"
    
GUI
=

![](readme/install1.png)

To start the GUI and execute the test with a nice GUI you need following command line arguments

    --project = relative path to the cypress directory located in this project. Simply "."
    --env = filename with basic settings where Cypress has to know. (file in fixtures folder)
    --config = some configurations Cypress need to start in the correct mode like the user agent for mobile testing
    
Schema:

    npx cypress open --detached --project "." --env configFile="#YOUR SETTINGS FILE YOU LIKE TO USE WITHOUT EXTENSION#" --config userAgent="smartfon"

Example start GUI without execute a test:

    Cyberpuerta Desktop
        npx cypress open --detached --env configFile="cp.tommy" --project "."
        
    Cyberpuerta Mobile
        npx cypress open --detached --env configFile="cp.tommy" --project "." --config userAgent="smartfon"
    
    Abasteo Desktop
        npx cypress open --detached --env configFile="ab.tommy" --project "."
        
Good to know
=

Wrong parameter
-

When you start Cypress, only tests which create for this configuration will run successfully. 
For example if you start with a wrong configuration file or forgot the user agent, mobile tests 
and tests which need a special configuration won't work.

Shortcut
-

To prevent problems, you can add the commands for GUI and CLI to the "package.json" file where you can
execute the tests by click the play button.

Reuse code
-

For reuse code, in the folder with all tests, there is a subfolder "inc" where some standard calls
to the shop implemented out of the box. A module in the shop receive for example a request
to create a new user which can use within the tests.

The file "inc/cypress.ts" contain all functions which global and can use in both shops or for the test itself.

In the folder "inc/cp/\*" contain all files which need CyberPuerta because of virtual click through the shop and the 
templates different from Abasteo. The same for Abasteo is in "inc/ab/\*".

For example the file "inc/cp/desktop/user.ts" contain a function "createUser" with all 
commands which Cypress need to "click" and "type" all information into the "registration" page.
This only work in Cyberpuerta Desktop and is a function assign logically to the user.

The file "inc/cp/mobile/user.ts" should contain all function which need to handle the mobile template.

The file "inc/cp/api/user.ts" contain functions which call the shop directly and create a new user within
the database without let Cypress click around because for example, we need a valid user for the tests, but it 
isn´t necessary to create a user within the shop frontend. Means this file contain all function
which not interact with the templates.

Schema:

    inc/#SHOP#/#TYPE LIKE api|desktop|mobile#/#LOGIC MODEL#.ts
    
So you can now reuse the present functions in your test file depending on the test you write:

    import * as cpcypress from "./inc/cp/cypress";
    import * as user from './inc/cp/desktop/user';
    import * as userApi from './inc/cp/api/user';
    import * as product from './inc/cp/desktop/product';
